#pragma once

#include <array>
#include <boost/numeric/odeint.hpp>

namespace odeint = boost::numeric::odeint;

template <class F>
class System
{
public:
    F g;
    F L01, L12;
    F m1, m2;

public:
    using state_type = std::array<F, 4>;

    System(F gravity) : g {gravity} { }
    System() : g {9.8} { }

    void operator()(const state_type & x, state_type & dxdt, const F & /*t*/)
    {
        dxdt[0] = x[2];
        dxdt[1] = x[3];

        F sint1 = sin(x[0]);
        F sint2 = sin(x[1]);
        F t1mt2 = x[0] - x[1];
        F sint1mt2 = sin(t1mt2);
        F cost1mt2 = cos(t1mt2);

        dxdt[2] = (g/L01 * (sint2*cost1mt2 - (1+m1/m2)*sint1)
                   - x[3]*x[3] * L12/L01 * sint1mt2
                   - x[2]*x[2] * sint1mt2 * cost1mt2)
                / (1 + m1/m2 - cost1mt2*cost1mt2);
        dxdt[3] = -dxdt[2]*L01/L12*cost1mt2 + x[2]*x[2]*L01/L12*sint1mt2 - g/L12 * sint2;
    }
};
