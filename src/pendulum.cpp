#include <glad/glad.h>
#include <GLFW/glfw3.h>
#include <imgui.h>
#include <imgui_internal.h>
#define GLM_FORCE_SWIZZLE
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

#include <memory>
#include <cmath>
#include <cstdio>

#include "imgui_impl_glfw_gl3.h"
#include "system.hpp"



using System_t = System<double>;
using error_stepper = odeint::runge_kutta_dopri5<System_t::state_type>;
System_t g_system;
System_t::state_type g_state;
using stepper_type = decltype(odeint::make_dense_output<error_stepper>(0.0, 0.0));
using iter_type = decltype(odeint::make_const_step_time_iterator_begin(odeint::make_dense_output<error_stepper>(0.0, 0.0), g_system, g_state, 0.0, 0.0, 0.0));

void InitGL();
void CreateVertices();
GLuint CreateShader(const char *, const char *);
void DoGui(iter_type & iter, stepper_type & stepper);
void RenderPendulum(GLFWwindow*, const System_t&, const System_t::state_type&);

GLuint g_vao, g_vbo;
GLuint g_shader_id;
GLuint g_circle_shader_id;
GLint g_shader_model_loc;
GLint g_shader_projection_loc;
GLint g_shader_color_loc;
constexpr size_t circle_num_vert = 32;

bool g_running = false;
float g_circle_size = 10.f;
float g_bar_width = 2.5f;
glm::vec4 g_ccol(0.6f, 0.6f, 0.6f, 1.0f);
glm::vec4 g_bcol(0.4f, 0.2f, 0.2f, 1.0f);
glm::vec4 g_bg_color(0.0f, 0.0f, 0.0f, 1.0f);

int main()
{
    if (!glfwInit())
    {
        return 1;
    }

    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
    glfwWindowHint(GLFW_CLIENT_API, GLFW_OPENGL_API);
    glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GLFW_TRUE);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
    glfwWindowHint(GLFW_VISIBLE, GLFW_FALSE);

    GLFWwindow * window = glfwCreateWindow(1280, 720, "Pendulum sim", nullptr, nullptr);

    if (!window)
    {
        glfwTerminate();
        return 1;
    }

    glfwMakeContextCurrent(window);
    gladLoadGLLoader((GLADloadproc) glfwGetProcAddress);

    InitGL();

    glfwSwapInterval(1);

    ImGui::CreateContext();
    ImGui_ImplGlfwGL3_Init(window, true, "#version 330");
    ImGui::StyleColorsDark();

    glfwShowWindow(window);

    auto stepper = odeint::make_dense_output<error_stepper>(1.0e-12, 1.0e-8);
    g_system.L01 = 1.0;
    g_system.L12 = 0.9;
    g_system.m1 = 1.0;
    g_system.m2 = 1.1;
    g_state[0] = M_PI;
    g_state[1] = 1.0;
    g_state[2] = 0.0;
    g_state[3] = 0.0;

    iter_type iter = odeint::make_const_step_time_iterator_begin(stepper, g_system, g_state, 0.0, 1.0, 1.0/60);
    iter_type iter_end = odeint::make_const_step_time_iterator_end(stepper, g_system, g_state);

    while (!glfwWindowShouldClose(window))
    {
        glfwPollEvents();
        ImGui_ImplGlfwGL3_NewFrame();

        DoGui(iter, stepper);


        auto [current_state, t] = *iter;

        RenderPendulum(window, g_system, current_state);

        ImGui::Render();
        ImGui_ImplGlfwGL3_RenderDrawData(ImGui::GetDrawData());

        glfwSwapBuffers(window);

        if (g_running)
        {
            ++iter;
            if (iter == iter_end)
            {
                iter = odeint::make_const_step_time_iterator_begin(stepper, g_system, g_state, 0.0, 1.0, 1.0/60);
            }
        }
    }

    glfwDestroyWindow(window);
    glfwTerminate();
    return 0;
}



void RenderPendulum(GLFWwindow * window, const System_t & sys, const System_t::state_type & s)
{
    float t1 = s[0];
    float t2 = s[1];

    int FBWidth, FBHeight;
    glfwGetFramebufferSize(window, &FBWidth, &FBHeight);
    glViewport(0, 0, FBWidth, FBHeight);

    float scale = FBWidth < FBHeight ? FBWidth : FBHeight;
    scale /= (sys.L01 + sys.L12) * 2.2f;

    glm::mat4 model, projection;
    glm::mat4 pos[3];

    pos[0] = glm::mat4(1.f);
    pos[1] = glm::translate(glm::rotate(pos[0], t1, glm::vec3(0, 0, 1)), glm::vec3(0, -g_system.L01*scale, 0));
    pos[2] = glm::translate(glm::rotate(pos[1], t2-t1, glm::vec3(0, 0, 1)), glm::vec3(0, -g_system.L12*scale, 0));

    glClearColor(g_bg_color.r, g_bg_color.g, g_bg_color.b, g_bg_color.a);
    glClear(GL_COLOR_BUFFER_BIT);

    projection = glm::ortho<float>(-FBWidth*.5f, FBWidth*.5f, -FBHeight*.5f, FBHeight*.5f);


    glUseProgram(g_shader_id);
    glUniformMatrix4fv(g_shader_projection_loc, 1, GL_FALSE, glm::value_ptr(projection));
    glUniform4f(g_shader_color_loc, g_bcol.r, g_bcol.g, g_bcol.b, g_bcol.a);

    model = glm::scale(glm::translate(pos[1], glm::vec3(0, g_system.L01 * scale * 0.5f, 0)), glm::vec3(g_bar_width, g_system.L01 * scale * 0.5f, 1));
    glUniformMatrix4fv(g_shader_model_loc, 1, GL_FALSE, glm::value_ptr(model));
    glBindVertexArray(g_vao);
    glDrawArrays(GL_TRIANGLE_FAN, 0, 4);
    glBindVertexArray(0);

    model = glm::scale(glm::translate(pos[2], glm::vec3(0, g_system.L12 * scale * 0.5f, 0)), glm::vec3(g_bar_width, g_system.L12 * scale * 0.5f, 1));
    glUniformMatrix4fv(g_shader_model_loc, 1, GL_FALSE, glm::value_ptr(model));
    glBindVertexArray(g_vao);
    glDrawArrays(GL_TRIANGLE_FAN, 0, 4);
    glBindVertexArray(0);


    glUseProgram(g_circle_shader_id);
    glUniformMatrix4fv(g_shader_projection_loc, 1, GL_FALSE, glm::value_ptr(projection));
    glUniform4f(g_shader_color_loc, g_ccol.r, g_ccol.g, g_ccol.b, g_ccol.a);

    model = glm::scale(pos[0], glm::vec3(g_circle_size, g_circle_size, 1));
    glUniformMatrix4fv(g_shader_model_loc, 1, GL_FALSE, glm::value_ptr(model));
    glBindVertexArray(g_vao);
    glDrawArrays(GL_TRIANGLE_FAN, 0, 4);
    glBindVertexArray(0);

    model = glm::scale(pos[1], glm::vec3(g_circle_size, g_circle_size, 1));
    glUniformMatrix4fv(g_shader_model_loc, 1, GL_FALSE, glm::value_ptr(model));
    glBindVertexArray(g_vao);
    glDrawArrays(GL_TRIANGLE_FAN, 0, 4);
    glBindVertexArray(0);

    model = glm::scale(pos[2], glm::vec3(g_circle_size, g_circle_size, 1));
    glUniformMatrix4fv(g_shader_model_loc, 1, GL_FALSE, glm::value_ptr(model));
    glBindVertexArray(g_vao);
    glDrawArrays(GL_TRIANGLE_FAN, 0, 4);
    glBindVertexArray(0);
}



static const char * shader_vert = R"GLSL(
    #version 330 core
    layout (location = 0) in vec2 vertex;

    uniform mat4 model, projection;

    void main()
    {
        gl_Position = projection * model * vec4(vertex, 0.0, 1.0);
    }
)GLSL";

static const char * shader_frag = R"GLSL(
    #version 330 core
    out vec4 outColor;

    uniform vec4 color;

    void main()
    {
        outColor = color;
    }
)GLSL";

static const char * circle_shader_vert = R"GLSL(
    #version 330 core
    layout (location = 0) in vec2 vertex;

    uniform mat4 model, projection;
    out vec2 pos;

    void main()
    {
        pos = vertex;
        gl_Position = projection * model * vec4(vertex, 0.0, 1.0);
    }
)GLSL";

static const char * circle_shader_frag = R"GLSL(
    #version 330 core
    in vec2 pos;
    out vec4 outColor;

    uniform vec4 color;

    void main()
    {
        float r = 0.0, delta = 0.01, alpha = 1.0;
        r = sqrt(dot(pos, pos));
        delta = fwidth(r);
        alpha = 1.0 - smoothstep(1.0 - delta, 1.0 + delta, r);
        outColor = color * alpha;
    }
)GLSL";



void InitGL()
{
    g_shader_id = CreateShader(shader_vert, shader_frag);
    g_circle_shader_id = CreateShader(circle_shader_vert, circle_shader_frag);
    g_shader_model_loc = glGetUniformLocation(g_shader_id, "model");
    g_shader_projection_loc = glGetUniformLocation(g_shader_id, "projection");
    g_shader_color_loc = glGetUniformLocation(g_shader_id, "color");
    CreateVertices();
}

void CreateVertices()
{
    glGenVertexArrays(1, &g_vao);
    glGenBuffers(1, &g_vbo);

    float vertices[] = {-1.f, -1.f, -1.f, 1.f, 1.f, 1.f, 1.f, -1.f};

    glBindVertexArray(g_vao);
    glBindBuffer(GL_ARRAY_BUFFER, g_vbo);
    glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);
    glVertexAttribPointer(0, 2, GL_FLOAT, false, 2 * sizeof(float), 0);
    glEnableVertexAttribArray(0);
    glBindVertexArray(0);
}

GLuint CreateShader(const char * vert, const char * frag)
{
    GLuint shader_id, vert_id, frag_id;
    GLint success;

    vert_id = glCreateShader(GL_VERTEX_SHADER);
    glShaderSource(vert_id, 1, &vert, nullptr);
    glCompileShader(vert_id);
    glGetShaderiv(vert_id, GL_COMPILE_STATUS, &success);
    if (!success)
    {
        fprintf(stderr, "Vertex shader compilation failed\n");
        exit(1);
    }

    frag_id = glCreateShader(GL_FRAGMENT_SHADER);
    glShaderSource(frag_id, 1, &frag, nullptr);
    glCompileShader(frag_id);
    glGetShaderiv(frag_id, GL_COMPILE_STATUS, &success);
    if (!success)
    {
        fprintf(stderr, "Fragment shader compilation failed\n");
        exit(1);
    }

    shader_id = glCreateProgram();
    glAttachShader(shader_id, vert_id);
    glAttachShader(shader_id, frag_id);
    glLinkProgram(shader_id);
    glGetProgramiv(shader_id, GL_LINK_STATUS, &success);
    if (!success)
    {
        fprintf(stderr, "Shader linking failed\n");
        exit(1);
    }

    glDeleteShader(vert_id);
    glDeleteShader(frag_id);

    return shader_id;
}

void DoGui(iter_type & iter, stepper_type & stepper)
{
    //ImGui::ShowDemoWindow();
    ImGui::SetNextWindowSizeConstraints(ImVec2(200, -1), ImVec2(1000, 1000));
    if (ImGui::Begin("Settings", nullptr,
        ImGuiWindowFlags_NoResize |
        ImGuiWindowFlags_AlwaysAutoResize |
        ImGuiWindowFlags_NoSavedSettings))
    {
        char playpause[32];
        sprintf(playpause, "%s###playpause", g_running ? "Pause" : "Play");
        if (ImGui::Button(playpause))
        {
            g_running = !g_running;
            if (g_running)
            {
                iter = odeint::make_const_step_time_iterator_begin(stepper, g_system, g_state, 0.0, 1.0, 1.0/60);
            }
        }
        if (ImGui::CollapsingHeader("Starting Conditions"))
        {
            if (g_running)
            {
                ImGui::PushItemFlag(ImGuiItemFlags_Disabled, true);
                ImGui::PushStyleVar(ImGuiStyleVar_Alpha, ImGui::GetStyle().Alpha * 0.5f);
            }

            static float t[2];
            static float dt[2];
            t[0] = g_state[0]; t[1] = g_state[1];
            dt[0] = g_state[2]; dt[1] = g_state[3];
            ImGui::DragFloat2("Angle", t);
            ImGui::DragFloat2("Angular velocity", dt);
            if (!g_running)
            {
                g_state[0] = t[0]; g_state[1] = t[1];
                g_state[2] = dt[0]; g_state[3] = dt[1];
            }

            if (g_running)
            {
                ImGui::PopItemFlag();
                ImGui::PopStyleVar();
            }
        }
        if (ImGui::CollapsingHeader("Display Options"))
        {
            ImGui::ColorEdit3("Circle color", glm::value_ptr(g_ccol));
            ImGui::ColorEdit3("Bar color", glm::value_ptr(g_bcol));
            ImGui::ColorEdit3("Background color", glm::value_ptr(g_bg_color));
            ImGui::DragFloat("Bar width", &g_bar_width, .1f, 0.f, 20.f);
            ImGui::DragFloat("Circle diameter", &g_circle_size, .1f, g_bar_width, 50.f);
        }
    }
    ImGui::End();
}
