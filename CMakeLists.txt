cmake_minimum_required(VERSION 3.2.0 FATAL_ERROR)
project(pendulum VERSION 0.1 LANGUAGES C CXX)


add_subdirectory(lib)

set(CMAKE_MODULE_PATH ${CMAKE_MODULE_PATH} ${CMAKE_SOURCE_DIR}/cmake)

find_package(GMP REQUIRED)
find_package(MPFR REQUIRED)
find_package(Boost REQUIRED)

add_executable(pendulum
    src/pendulum.cpp
    src/glad.c

    lib/imgui/imgui.cpp
    lib/imgui/imgui_draw.cpp
    lib/imgui/imgui_demo.cpp
    src/imgui_impl_glfw_gl3.cpp
)

target_include_directories(pendulum
    PRIVATE
        include
        lib/imgui
        ${GMP_INCLUDE_DIR}
        ${MPFR_INCLUDE_DIR}
)

target_link_libraries(pendulum
    glfw
    glm
    ${GMP_LIBRARIES} ${GMPXX_LIBRARIES}
    ${MPFR_LIBRARIES}
    Boost::boost
)

set_property(TARGET pendulum PROPERTY CXX_STANDARD 17)
set_property(TARGET pendulum PROPERTY CXX_STANDARD_REQUIRED ON)
